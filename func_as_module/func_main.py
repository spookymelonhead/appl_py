import func

userName = ["John Doe"]

#passing list as argument, changes made to list are reflected into list
func.input_userName(userName)
func.greet_user(userName[1])

#passing list as argument, changes made to list are NOT reflected into list
func.input_userName(userName[:])
func.greet_user(userName[1])


#passing variable no of arguments
func.make_pizza("Jalapeneos", "pepperoni", "Tandoori Chicken")
func.make_pizza(18, "Jalapeneos", "pepperoni", "Tandoori Chicken", "Tandoori sauce", "schezwan sauce")


profile=func.build_profile('Bruce', 'Wayne', age=31, location='Gotham')
print (profile)