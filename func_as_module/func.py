def input_userName(name):
	"""Takes username as input"""
	name.append(input("Enter name: "))

def greet_user(name):
	"""greets user"""
	print("Hi, " + name)

def make_pizza(*toppings):
	print(toppings)

def make_pizza(size, *toppings):
	print(size)
	print(toppings)

#passing variable no of key and value pair
def build_profile(first, last, **info):
	"""Build a dictionary for a persons profile and info. And returns the dictionary"""
	profile={}
	profile['first_name']=first
	profile['last_name']=last

	for key, value in info.items():
		profile[key]=value
	return profile
