#open file read its content and print
with open('file') as fd:
	content=fd.read()
	print(content)
#open file read line by line and print
with open('file') as fd:
	for line in fd:
		print(line.rstrip())

print ('\n')
#reading and storing lines of file in a list
FILE_NAME='file'
with open(FILE_NAME) as fd:
	lines=fd.readlines()
pi_string=''
for line in lines:
	print (line.rstrip())
	pi_string +=line.rstrip()
print('Pi String: ')
print(pi_string)

#looking for a particular string in a string
search='3229'
if search in pi_string:
	print('Yes found it.!!')
else:
	print('not found..!!')

with open(FILE_NAME, 'a') as fd:
	fd.write("\nbecause I'm batman. ~Batman")