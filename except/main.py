#try except block
try:
	print(69/0)
except:
	print("Exception. Division by ZERO")
else:
	print("Success")

try:
	print(69/13)
except:
	print("Exception. Division by ZERO")
else:
	print("Success")

try:
	print(169/0)
except:
	pass
else:
	print("Success")	