from car import Car, Electric, Battery

class Animal():
	"""Defines class Animal, Parent of Dog and other animals"""
	def __init__(self, animal_type):
		self.animal_type=animal_type
		print ('Parent class constructor')
	def return_animal_type(self):
		return (self.animal_type)

class Dog(Animal):
	"""Dog Class to model a dog"""
	def __init__(self,animal_type,name,age,breed):
		"""Initializes Dog class object with Name and age"""
		super().__init__(animal_type)
		self.name=name
		self.age=age
		self.breed=breed
		self.bark_count=0
		self.skills = []
		print ('Derived class constructor')
	def bark(self):
		self.bark_count = self.bark_count + 1
		print(self.name.title() + " says bhou whow")
	def set_skills(self, *skills):
		self.skills = skills

duffy=Dog("doggo","duffy", 8, "dachshund")
duffy.set_skills("playfull", "jumpy", "hillarious", "cute")
print("Dogs name " + duffy.name.title())
print("Dogs age " + str(duffy.age))
print("Dogs breed " + duffy.breed)
print(duffy.name.title() +"'s skills are ")
print(duffy.skills)
duffy.bark()
duffy.bark()
duffy.bark()
print(duffy.name.title() +" has barked "+str(duffy.bark_count)+ " times")
duffy.bark()
print(duffy.name.title() + " is animal of type " + duffy.return_animal_type())




roadster=Electric("Roadster", "Tesla", "2020", "Electric", "7400", "280", "250000")

print("Printing Roadster info:")
print("Model: ", roadster.model)
print("Make: ", roadster.make)
print("Year: ", roadster.year)

print("Torque: ", roadster.torq)
print("Miles per charge: ", roadster.milesCap)
print("Battery Capacity: ", roadster.battery.batteryCap)
