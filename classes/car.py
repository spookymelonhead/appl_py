class Car():
	def __init__(self, model, make, year, carType, torq):
		self.model=model
		self.make=make
		self.year=year
		self.type=carType
		self.torq=torq
	def ignitionON(self):
		self.ignition=1
	def ignitionOFF(self):
		self.ignition=0

class Battery():
	def __init__(self, batteryCap):
		self.batteryCap = batteryCap

class Electric(Car):
	def __init__(self, model, make, year, carType, torq, milesCap, batteryCap):
		super().__init__(model, make, year, carType, torq)
		self.milesCap=milesCap
		#class instance inside a class
		self.battery=Battery(batteryCap)