#upper case lower case title
name="bhasma Sur"
print (name.title())
print (name.upper())
print (name.lower())

#string concatinate
fName="jack"
lName="sparrow"
fullName=fName + " " + lName
print(fName+" "+lName)
print(fullName.title())

#whitespaces
fName="bruce\t"
lName="wayne\t\n"
print(fName + lName)

#strip whitespaces
fname="			Brian doobmar		"
print(fname.strip())

#concatinate int with strings
print("Brian is "+str(25)+" years old right now")