import json

FILE_USR_CREDENTIALS='cred.json'

userid=''
passwd=''
cred_available='FALSE'

try:
	with open(FILE_USR_CREDENTIALS, "r") as fd_cred:
		print('Loading JSON..!!')
		json_data=json.load(fd_cred)
		if "TRUE" == json_data['cred_available']:
			print(json_data['userid'])
			print(json_data['passwd'])
		else:
			print('JSON string holds Junk data')
			raise Exception('JSON string holds Junk data')
except:
	with open(FILE_USR_CREDENTIALS, "w+") as fd_cred:
		print('New credentials required')
		userid=input('Please enter your new User ID: ')
		passwd=input('Please enter a strong password: ')
		cred_available='TRUE'
		data= {'cred_available' : cred_available, 'userid' : userid, 'passwd': passwd}
		json.dump(data, fd_cred)
		print('Credentials saved. Thanks..!!')