#generating list
for value in range(1,10):
	print(value)

#list of natural nos
natural_nos= list(range(1,10))
print(natural_nos)

#print even nos, nos b/w [2,11) with increment of 2
even_nos=list(range(2,11,2))
print(even_nos)

#sq of natural nos
squares=list(range(0,11))
for num in squares:
	squares[num]= num**2

print (squares)

#squares of natural nos by list comprehension
squares= [num**2 for num in range(0,11)]
print (squares)

colors=["purple", "blue", "pink", "magenta", "santra", "peela"]
en_colors=colors[0:4]
print(en_colors)
#or
en_colors=colors[:4]
print(en_colors)

hindi_colors=colors[4:]
print(hindi_colors)

#negative index displays element from the last of the list
print(en_colors[-1:])
print(en_colors[-2:])

#copy a list from existing list
new_colors = en_colors[0:3]
new_colors.append("burgandy")
print(new_colors)


#TUPLES, tuples are like non edicatble lists, but they can be redefined
PI=(3.0, 0.1459)

print("area of the circle with radii 2 is", (PI[0] + PI[1]) * (2**2) )

#below like would give error, as it tries to modufy tuple
#PI[0]= 2.0

PI=(3.0, 0.145987)
print("area of the circle with radii 2 is", (PI[0] + PI[1]) * (2**2) )

available_toppings=["extra cheese", "jalapeneos", "tandoori chicken", "pepperoni"]
requested_toppings=["mushrooms", "extra cheese", "tandoori chicken"]

for topping in requested_toppings:
	if topping in available_toppings:
		print("adding " +topping+ " to your pizza..!!")
	else:
		print(topping + " not avaialble..Sorry..!!")
print ("Finished making your pizza..Yay..!!")