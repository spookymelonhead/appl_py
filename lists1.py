dogs=["jack russel terrier", "Labrador Retriever", "golden retriever", "pug", "bloodhound", "desi kutta"]

print(dogs[0].title())
#-1 index displays last element in list
print(dogs[-1].title())

#add new
print("add new")
dogs.append("dhobhi ka kutta")
print(dogs[-1].title())
print("\n")

#insert new, -1 not working below as i expected..!!
print("insert new, -1 not working below as i expected..!!")
dogs.insert(-1, "na ghar ka na ghat ka")
print(dogs)

#delete from list
del dogs[3]
print(dogs)

#deleted element frm list might be required again that case use pop
popped_dogs = dogs.pop() #last element popped
print(dogs)
print(popped_dogs)

#remove by value
dogs.remove("bloodhound")
print(dogs)


#sorting just to print
print(sorted(dogs))
#sorting
print(dogs)
dogs.sort()
print(dogs)
dogs.sort(reverse=True)
print(dogs)
