batman={"name":"bruce wayne", "age": "31", "skills": "mma, martial arts, jujitsu"}
print(batman)

print(batman["name"])
del(batman["age"])
print(batman)


#usefull for lookup up tabels I think
fav_languages={
	'brian': 'C, C++, Python, Bash',
	'ankit': 'C++, C',
	'aditya': 'Python, C, C++', 
}
print(fav_languages)

for key, value in fav_languages.items():
	print("Key:" + key)
	print("Value:" + value)