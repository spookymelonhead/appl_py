import pygame
import enum
from settings import SHIP_RESOLUTION

class Ship():
	"""Ship class to model a ship. Loaded from a image file"""
	"""initializ ship, starting position"""
	def __init__(self, screen, settings):
		#Load ship's image
		self.image=pygame.image.load(settings.ship.falcon_ship_img)
		self.image=pygame.transform.flip(self.image, settings.ship.falcon_ship_conf[1], settings.ship.falcon_ship_conf[2])
		self.image=pygame.transform.scale(self.image, settings.ship.falcon_ship_size)
		#Any shape can be represented as rectangle
		self.rect=self.image.get_rect() #ships image rect
		self.screen_rect=screen.get_rect() #screens rect

		if settings.ship.falcon_ship_conf[0] == 'bottom':
			self.rect.bottom = self.screen_rect.bottom
		elif settings.ship.falcon_ship_conf[0] == 'top':
			self.rect.top = self.screen_rect.top

		# Starting Position ship at the bottom center of the screen.
		self.rect.centerx = self.screen_rect.centerx

	def move_right(self, settings):
		print('incrementing ', settings.motion_step)
		# Move the ship to the right.
		self.rect.centerx += settings.motion_step
		if self.rect.centerx > 1095:
			self.rect.centerx = 1095

	def move_left(self, settings):
		print('incrementing ', settings.motion_step)
		# Move the ship to the right.
		self.rect.centerx -= settings.motion_step
		if self.rect.centerx < 95:
			self.rect.centerx = 95

	def blitme(self, screen, settings):
		"""Draw ship at its current location"""
		screen.blit(self.image, self.rect)