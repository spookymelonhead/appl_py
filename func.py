userName = ["John Doe"]

def input_userName(name):
	"""Takes username as input"""
	name.append(input("Enter name: "))

def greet_user():
	"""greets user"""
	global userName
	print(userName)

def make_pizza(*toppings):
	print(toppings)

def make_pizza(size, *toppings):
	print(size)
	print(toppings)

#passing variable no of key and value pair
def build_profile(first, last, **info):
	"""Build a dictionary for a persons profile and info. And returns the dictionary"""
	profile={}
	profile['first_name']=first
	profile['last_name']=last

	for key, value in info.items():
		profile[key]=value
	return profile

#passing list as argument, changes made to list are reflected into list
input_userName(userName)
greet_user()

#passing list as argument, changes made to list are NOT reflected into list
input_userName(userName[:])
greet_user()


#passing variable no of arguments
make_pizza("Jalapeneos", "pepperoni", "Tandoori Chicken")
make_pizza(18, "Jalapeneos", "pepperoni", "Tandoori Chicken", "Tandoori sauce", "schezwan sauce")


profile=build_profile('Bruce', 'Wayne', age=31, location='Gotham')
print (profile)

